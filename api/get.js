import chrome from '@sparticuz/chromium'
import puppeteer from 'puppeteer-core'


export default async function handler(request, response) {

    const browser = await puppeteer.launch({
        args: chrome.args,
        defaultViewport: chrome.defaultViewport,
        executablePath: await chrome.executablePath(),
        headless: 'new',
        ignoreHTTPSErrors: true
    })

    // Check if url provided and valid
    const url = decodeURIComponent(request.query.url)
    if (!url.length) {
        return response.json({
            error: 'The given URL is invalid'
        })
    }

    const page = await browser.newPage()
    await page.goto(url);
    await page.setViewport({ width: 1080, height: 1024 });

    const html = await page.content()

    response.setHeader('Access-Control-Allow-Credentials', true)
    response.setHeader('Access-Control-Allow-Origin', '*')
    return response.send(html);
}